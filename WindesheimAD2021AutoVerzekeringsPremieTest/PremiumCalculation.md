## Module: Testen

In dit bestand ga ik in het kort beschrijven waarom ik enkele test gedaan heb. Ik zal beschrijven waarom ik voor de data invulling heb gekozen,
en welke technieken ik heb gebruikt daarvoor.

## Vehicle

Voor mijn vehicle mock heb ik random gegevens gebruikt. Vermogen had ik ingesteld op 450kw, prijs ingesteld op 15.000 en het bouwjaar 1999.

## Policyholder

Voor de policyholder mock heb ik gekozen voor 28 jaar, 8 jaar rijbewijs, postcode 8345 en 0 geclaimde jaren. De reden voor deze gegevens was dat ik hierbij 
niet een premie opslag van 15% erbij hoefde te rekenen. Dat maakt de sommen wat makkelijker

## CalculateBasePremiumTest

Hierbij werd de basis premie berekent. Er stond een klein foutje in de hoofdcode, de berekening stond op "<=5" en dat moest worden lager dan 5 alleen.
Uiteindelijk moest het resultaat 72.67 worden, en dat was ook gelukt. De test werkte dus.

## PolicyHolderAgeOf23OrLicenseAgeUnder5YearsGet15PercentHigherPremiumTest

Hierbij werd berekent of de Policyholder een extra toeslag zou moeten betalen als die onder de leeftijd was van 23, of zijn rijbewijs minder dan 5 jaar
in bezet heeft. Hierbij heb ik een Theory met InlineData gebruikt ipv Facts, zodat ik verschillende data tegelijk zou kunnen runnen. Hier heb ik verschillende
scenario's getest, met min en max scenario's. Hiermee bedoel ik dat ik de grenzen van de leeftijd en rijbewijsleeftijd heb getest. De testjes verliepen goed
en hij gaf daadwerkelijk aan wanneer iemand een extra toeslag moest betalen.

Ik heb actualOutcome een variable gemaakt actualOutcomeDouble om decimalen af te ronden met 2 cijfers achter de komma, zoals bij een normaal geldbedrag.

## CheckForNoClaimYears6YearsOrAboveGetDiscountsTest

Hierbij werd getest of de Policyholder een korting krijgt wanneer hij 6 of meer jaar geen schade heeft geclaimt. Zelfde als hierboven heb ik met de hulp van
Theory en InlineData alle scenario's getest. De grenzen van schadevrije jaren getest. En ook hierbij verliepen de testjes goed.


## CheckPostalCodeForCodesBetweenCertainPostalCodesForExtraCost

Hierbij werd gekeken of bepaalde postcodes meer opslag moesten gaan betalen dan andere postcodes. Hier had ik ook de Theory gebruikt met verschillende values,
en hier heb ik ook de doubleExpectation ervoor gebruikt. Na verschillende tests te hebben gerund klopte de data inderdaad zoals in de opdracht beschreven.

## CheckIfDifferentCoveragesGiveCorrectResult

Hierbij werd gekeken of de verschillende coverages de juiste resultaten gaven. Hiervoor heb ik "Fact" gebruikt ipv Theory, omdat er nu niet met verschillende
scenario's getest hoeft te worden. Voor alle 3 berekeningen heb ik ook weer de rounded var gebruikt, om de bedragen afteronden 2 decimalen achter de komma.

## YearlyPaymentSubtotalWithDiscountTest & MonthlyPaymentSubtotalWithoutDiscountTest

Ook bij deze 2 formules heb ik Fact gebruikt ipv Theory, en de bedragen werden beide 2 cijfers achter de komma afgerond. 

## Stryker

Toen ik klaar was met alle testen heb ik Stryker gebruikt om te kijken of er nog mutanten aanwezig waren die ik zou kunnen oplossen. 90% was er inderdaad
uitgehaald al, alleen waren er nog een aantal over, die heb ik helaas niet eruit kunnen halen en kunnen oplossen.



Bedankt voor alle leerzame lessen, geduld en leuke uitleg. Het was een leuke maar wel uitdagende opdracht!
