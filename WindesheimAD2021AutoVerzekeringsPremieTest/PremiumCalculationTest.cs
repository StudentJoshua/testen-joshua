using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Moq;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {

        private static Mock<Vehicle> MockVehicle(int powerInKW = 450)
        {
            var mockVehicle = new Mock<Vehicle>(450, 15000, 1999);
            mockVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(powerInKW);
            mockVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(15000);
            mockVehicle.Setup(vehicle => vehicle.Age).Returns(22);
            return mockVehicle;
        }

        private static Mock<PolicyHolder> MockPolicyHolder(int age = 28, int licenseAge = 8, int postalCode = 8345, int noClaimYears = 0)
        {
            var mockPolicyHolder = new Mock<PolicyHolder>(MockBehavior.Default, 26, "01-01-1999", 8239, 0);
            mockPolicyHolder.Setup(policyholder => policyholder.Age).Returns(age);
            mockPolicyHolder.Setup(policyholder => policyholder.LicenseAge).Returns(licenseAge);
            mockPolicyHolder.Setup(policyholder => policyholder.PostalCode).Returns(postalCode);
            mockPolicyHolder.Setup(policyholder => policyholder.NoClaimYears).Returns(noClaimYears);
            return mockPolicyHolder;
        }

        private static readonly double BasePremium = 72.666666667;

        [Fact]
        public void CalculateBasePremiumTest()
        {
            //Arrange
            Vehicle car = new Vehicle(450, 15000, 1999);
            double expectedResult = 72.67;

            //Act
            double basePremium = Math.Round(PremiumCalculation.CalculateBasePremium(car),2);

            //Assert
            Assert.Equal(expectedResult, basePremium);
        }

        [Theory]
        [InlineData(25, 5, 1.00)]
        [InlineData(26, 2, 1.15)]
        [InlineData(22, 5, 1.15)]
        [InlineData(30, 10, 1.00)]
      
        public void PolicyHolderAgeOf23OrLicenseAgeUnder5YearsGet15PercentHigherPremiumTest(int age, int licenseAge, double expectation)
        {
            //Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder(age, licenseAge).Object;

            //Act
            var actualOutcome = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var actuaOutcomeDouble = Math.Round(actualOutcome.PremiumAmountPerYear, 2);
            var expectedMultiplier = Math.Round(expectation * BasePremium, 2);

            //Assert
            Assert.Equal(expectedMultiplier, actuaOutcomeDouble);
        }

        [Theory]
        [InlineData(4, 1.00)]
        [InlineData(5, 1.00)]
        [InlineData(6, 0.95)]
        [InlineData(11, 0.70)]
        [InlineData(17, 0.40)]
        [InlineData(18, 0.35)]
        [InlineData(19, 0.35)]


        public void CheckForNoClaimYears6YearsOrAboveGetDiscountsTest(int noClaimYears, double expectation)
        {
            //Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder(noClaimYears : noClaimYears).Object;

            //Act
            var actualOutcome = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var actuaOutcomeDouble = Math.Round(actualOutcome.PremiumAmountPerYear, 2);
            var expectedMultiplier = Math.Round(expectation * BasePremium, 2);

            //Assert
            Assert.Equal(expectedMultiplier, actuaOutcomeDouble);
        }

        [Theory]
        [InlineData(1334, 1.05)]
        [InlineData(3600, 1.02)]
        [InlineData(3599, 1.05)]
        [InlineData(1000, 1.05)]
        [InlineData(4499, 1.02)]
        [InlineData(4500, 1.00)]


        public void CheckPostalCodeForCodesBetweenCertainPostalCodesForExtraCosts(int postalCode, double expectation)
        {
            //Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder(postalCode: postalCode).Object;

            //Act
            var actualOutcome = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var actuaOutcomeDouble = Math.Round(actualOutcome.PremiumAmountPerYear, 2);
            var expectedMultiplier = Math.Round(expectation * BasePremium, 2);

            //Assert
            Assert.Equal(expectedMultiplier, actuaOutcomeDouble);
        }

        [Fact]
        public void CheckIfDifferentCoveragesGiveCorrectResults()
        {
            //Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder().Object;
            double WA = 72.67;
            double WA_PLUSexpected = 87.20;
            double ALL_RISKexpected = 145.33;

            //Act
            var outcomeWA = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA);
            var roundedWA = Math.Round(outcomeWA.PremiumAmountPerYear, 2);
            var outcomeWA_PLUS = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA_PLUS);
            var roundedWA_PLUS = Math.Round(outcomeWA_PLUS.PremiumAmountPerYear, 2);
            var outcomeALL_RISK = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.ALL_RISK);
            var roundedALL_RISK = Math.Round(outcomeALL_RISK.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(WA, roundedWA);
            Assert.Equal(WA_PLUSexpected, roundedWA_PLUS);
            Assert.Equal(ALL_RISKexpected, roundedALL_RISK);
        }

        [Fact]
        public void YearlyPaymentSubtotalWithDiscountTest()
        {
            //Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder().Object;
            double expectedOutcomeYearly = 73;

            //Act
            var actualOutcomeYearly = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //Assert
            Assert.Equal(expectedOutcomeYearly, Math.Round(actualOutcomeYearly * 1.025));
        }

        [Fact]
        public void MonthlyPaymentSubtotalWithoutDiscountTest()
        {
            //Arrange
            var mockVehicle = MockVehicle().Object;
            var mockPolicyHolder = MockPolicyHolder().Object;
            double expectedMonthlyOutcome = 6.06;

            //Act
            var actualMonthlyOutcome = new PremiumCalculation(mockVehicle, mockPolicyHolder, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            //Assert
            Assert.Equal(expectedMonthlyOutcome, actualMonthlyOutcome);
        }
    }
}
